import React from 'react';
import { View, Text} from 'react-native';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import * as ImagePicker from 'expo-image-picker';

import CameraToolbar from './cameraToolbar'

import styles from './styles';


export default class CameraPage extends React.Component {
    camera = null;

    state = {
        hasCameraPermission: null,
        captures:[],
        flashMode: Camera.Constants.FlashMode.off,
        cameraType: Camera.Constants.Type.back,
    };

    setFlashMode = (flashMode) => this.setState({flashMode});
    setCameraType = (cameraType) => this.setState({ cameraType });
    
    handleShortCapture = async () => {
        const photoData = await this.camera.takePictureAsync();
        this.setState({captures: [photoData, ...this.state.captures] })
    };

    async componentDidMount() {
        const camera = await Permissions.askAsync(Permissions.CAMERA);
        const gallery = await Permissions.askAsync(Permissions.MEDIA_LIBRARY)
        const hasCameraPermission = (camera.status === 'granted' && gallery.status === 'granted');

        this.setState({ hasCameraPermission });
    };


    selectImage() {
        let options = {
            title: 'Image Library',
            maxWidth: 256,
            maxHieght: 256,
            storageOptions: {
                skipBackup: true
            }
        };
  
        ImagePicker.launchImageLibraryAsync(options, response => {
            console.log({ response });
    
            if (response.didCancel) {
                console.log('User canelled photo picker');
                Alert.alert('You did not select an image')
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButon) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri:response.uri};
                console.log({ source });
            }
      });
    }

    render() {
        const { hasCameraPermission } = this.state;

        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>Access to camera has been denied.</Text>;
        }

        return (
            <>
                <View>
                    <Camera
                        style={styles.preview}
                        ref={camera => this.camera = camera}
                        type={this.state.cameraType}
                        flashMode={this.state.flashMode}
                        />
                </View>
                    <CameraToolbar 
                    flashMode={this.state.flashMode}
                    cameraType={this.state.cameraType}
                    setFlashMode={this.setFlashMode}
                    setCameraType={this.setCameraType}
                    onShortCapture={this.handleShortCapture}
                    gallery={this.selectImage} />
            </>
        );
    };
};