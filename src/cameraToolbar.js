import React from 'react';
import {Camera} from 'expo-camera';
import { Ionicons } from '@expo/vector-icons';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { View, TouchableOpacity } from 'react-native';

import styles from './styles';

const { FlashMode: CameraFlashModes, Type: CameraTypes } = Camera.Constants;



const CameraToolbar = (props) => {
    
    return(
        <Grid style = {styles.bottomToolbar}>
            <Row>
                <Col size = {2} style={styles.alignCenter}>
                    <TouchableOpacity
                        onPress={props.onShortCapture}
                    >
                        <Ionicons
                            name='ellipse-outline'
                            color='white'
                            size={100}
                        /> 
                    </TouchableOpacity>
                </Col>
            </Row>
            <Row>
                <Col style={styles.alignCenter}>
                    <TouchableOpacity onPress={() => props.setFlashMode(
                        props.flashMode === CameraFlashModes.on ? CameraFlashModes.off : CameraFlashModes.on
                    )}>
                        <Ionicons
                            name={props.flashMode == CameraFlashModes.on ? 'md-flash' : 'md-flash-off'}
                            color='white'
                            size={40}
                        /> 
                    </TouchableOpacity>
                </Col>
                <Col style = {styles.alignCenter}>
                    <TouchableOpacity onPress={props.gallery}>
                        <Ionicons
                            name='images-outline'
                            color="white"
                            size={40}
                            />
                    </TouchableOpacity>
                </Col>
                <Col style={styles.alignCenter}>
                <TouchableOpacity onPress={() => props.setCameraType(
                    props.cameraType === CameraTypes.back ? CameraTypes.front : CameraTypes.back
                )}>
                    <Ionicons
                        name='camera-reverse-outline'
                        color="white"
                        size={40}
                    />
                </TouchableOpacity>
                </Col>
            </Row>

        </Grid>
    )
}

export default CameraToolbar;
